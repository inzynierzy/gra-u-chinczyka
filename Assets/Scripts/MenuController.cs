﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Photon.Realtime;
using Photon.Pun;

namespace PACHISI
{
    public class MenuController : MonoBehaviourPunCallbacks
    {
        public InputField playerNameInput;

        private AudioManager audioManager;
        public Slider slider;

      

        #region Menu Dialogs
        [Header("DynamicCanvas")]
        public GameObject dynamicCanvas;

        public GameObject menuCanvas;

        public GameObject createRoomCanvas;
        public InputField roomNameInput;
        public Toggle publicVisibleToggle;

        public GameObject joinRoomCanvas;
        public GameObject roomListContent;
        public GameObject roomListPrefab;
        public InputField roomCodeInput;

        public GameObject optionsCanvas;
        #endregion

        private bool isMenuActive = true;

        private Dictionary<string, RoomInfo> cachedRoomList;

        private Dictionary<string, GameObject> roomList;

        #region MonoBehaviour CallBacks

        void Awake()
        {
            audioManager = FindObjectOfType<AudioManager>();
            if (!PlayerPrefs.HasKey(NickNameInput.nickNamePrefKey))
            {
                playerNameInput.text = "Player " + Random.Range(1000, 10000);
            } else
            {
                playerNameInput.text = PlayerPrefs.GetString(NickNameInput.nickNamePrefKey);
            }

            cachedRoomList = new Dictionary<string, RoomInfo>();
            roomList = new Dictionary<string, GameObject>();
        }

        void Start()
        {
            slider.value = PlayerPrefs.GetFloat(AudioManager.uiMusicVol, 1f);

            setVolumeInAllMixerGroups();
        }

       

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && !isMenuActive) BackToMenu();
        }

        private void OnDestroy()
        {
            saveUserVolume();

        }

        #endregion

        #region Photon CallBacks

        public override void OnLeftLobby()
        {
            cachedRoomList.Clear();

            ClearRoomListView();
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            ClearRoomListView();

            UpdateCachedRoomList(roomList);
            UpdateRoomListView();
        }

        #endregion

        #region Public Methods

        public void ClickSound()
        {
            GetComponent<AudioSource>().Play();
        }

        public void CreateRoom()
        {
            string roomName = "Room";
            if (roomNameInput.text != "")
            {
                roomName = roomNameInput.text;
            }
            else
            {
                roomName += Random.Range(1000, 10000);
            }
            bool isPublic = publicVisibleToggle.isOn;
            PhotonNetwork.CreateRoom(roomName, new RoomOptions { MaxPlayers = 4, IsVisible = isPublic });
        }

        public void JoinRoom()
        {
            string roomName = roomCodeInput.text;
            PhotonNetwork.JoinRoom(roomName);
        }

        public void JoinRandomRoom()
        {
            PhotonNetwork.JoinRandomRoom();
        }

        public void BackToMenu()
        {
            createRoomCanvas.SetActive(false);
            joinRoomCanvas.SetActive(false);
            optionsCanvas.SetActive(false);
            menuCanvas.SetActive(true);
            isMenuActive = true;
        }

        public void MouseClick(string buttonType)
        {
            switch (buttonType)
            {
                case "CreateRoom":
                    Debug.Log("<color=Blue>Create Room</color>");
                    createRoomCanvas.SetActive(true);
                    menuCanvas.SetActive(false);
                    break;
                case "JoinRoom":
                    Debug.Log("<color=Blue>Join Room</color>");
                    joinRoomCanvas.SetActive(true);
                    menuCanvas.SetActive(false);
                    break;
                case "JoinRandomRoom":
                    Debug.Log("<color=Blue>Join Random Room</color>");
                    break;
                case "Options":
                    Debug.Log("<color=Blue>Options</color>");
                    optionsCanvas.SetActive(true);
                    menuCanvas.SetActive(false);
                    break;
                case "Exit":
                    Debug.Log("<color=Blue>Exit</color>");
                    Application.Quit();
                    break;
            }
            isMenuActive = false;
        }

        private void ClearRoomListView()
        {
            foreach (GameObject entry in roomList.Values)
            {
                Destroy(entry.gameObject);
            }

            roomList.Clear();
        }

        private void UpdateCachedRoomList(List<RoomInfo> roomList)
        {
            foreach (RoomInfo info in roomList)
            {
                if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
                {
                    if (cachedRoomList.ContainsKey(info.Name))
                    {
                        cachedRoomList.Remove(info.Name);
                    }

                    continue;
                }

                if (cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList[info.Name] = info;
                }
                else
                {
                    cachedRoomList.Add(info.Name, info);
                }
            }
        }

        private void UpdateRoomListView()
        {
            foreach (RoomInfo info in cachedRoomList.Values)
            {
                GameObject entry = Instantiate(roomListPrefab);
                entry.transform.SetParent(roomListContent.transform);
                entry.transform.localScale = Vector3.one;
                entry.GetComponent<RoomList>().Initialize(info.Name, (byte)info.PlayerCount, info.MaxPlayers);

                roomList.Add(info.Name, entry);
            }
        }

        #endregion

        #region User Volume
        private void saveUserVolume()
        {
            saveVolumeToPlayerPrefs(AudioManager.uiMusicVol);
            saveVolumeToPlayerPrefs(AudioManager.uiSfxVol);
            saveVolumeToPlayerPrefs(AudioManager.gameMusicVol);
            saveVolumeToPlayerPrefs(AudioManager.gameSfxVol);
        }

        private void saveVolumeToPlayerPrefs(string name)
        {
            string uiMusicVol = name;
            bool result = audioManager.getAudioMixer().GetFloat(uiMusicVol, out float value);
            if (result)
            {
                PlayerPrefs.SetFloat(uiMusicVol, value);
                
            }
        }

        private void setVolumeInAllMixerGroups()
        {
            setMixerGroupVolume(AudioManager.uiMusicVol);
            setMixerGroupVolume(AudioManager.uiSfxVol);
            setMixerGroupVolume(AudioManager.gameMusicVol);
            setMixerGroupVolume(AudioManager.gameSfxVol);
        }

        private void setMixerGroupVolume(string mixerGroupName)
        {
            float volume = PlayerPrefs.GetFloat(mixerGroupName, 0.5f);
            audioManager.getAudioMixer().SetFloat(mixerGroupName, volume);
        }
        #endregion

        #region Slider
        public void setUiMuiscVolume(float vol)
        {
            audioManager.setUiMusicVolume(vol);
            
        }
        #endregion
    }
}