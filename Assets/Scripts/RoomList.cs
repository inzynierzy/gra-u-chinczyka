﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

namespace PACHISI
{
    public class RoomList : MonoBehaviour
    {
        public Text roomNameText;

        public Text roomPlayersText;

        public Button joinRoomBtn;

        private string roomName;

        #region MonoBehaviour CallBacks

        public void Start()
        {
            joinRoomBtn.onClick.AddListener(() =>
            {
                if (PhotonNetwork.InLobby) PhotonNetwork.LeaveLobby();

                PhotonNetwork.JoinRoom(roomName);
            });
        }

        public void Initialize(string name, byte currentPlayers, byte maxPlayers)
        {
            roomName = name;
            roomNameText.text = name;
            roomPlayersText.text = currentPlayers + " / " + maxPlayers;
        }

        #endregion
    }
}
