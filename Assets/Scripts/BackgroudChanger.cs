﻿using UnityEngine;

namespace PACHISI
{
    public class BackgroudChanger : MonoBehaviour
    {
        public Sprite[] sprites;

        public SpriteRenderer spriteRenderer;

        private float timer = 0.0f;

        private readonly float waitTime = 10.0f;

        #region MonoBehaviour CallBacks

        void Start()
        {

        }

        void Update()
        {
            timer += Time.deltaTime;
            if (timer > waitTime)
            {
                Debug.Log("<color=Red>Change Background</color>");
                spriteRenderer.sprite = sprites[Random.Range(0, sprites.Length)];

                this.timer -= timer;
            }
        }

        #endregion
    }
}
