﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PACHISI
{
    public class NetworkConnectionManager : MonoBehaviourPunCallbacks
    {
        bool isConnectingRoom = false;

        string gameVersion = "v0.1";

        #region MonoBehaviour CallBacks

        void Awake()
        {

        }
        void Start()
        {

        }

        void Update()
        {
            if (!PhotonNetwork.IsConnected) ConnectToMaster();
        }

        #endregion

        #region Photon CallBacks
        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            Debug.Log("<Color=Green>OnPlayerEnteredRoom(): " + newPlayer.NickName + "</Color>");
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            Debug.Log("<Color=Green>OnPlayerLeftRoom(): " + otherPlayer.NickName + "</Color>");
        }

        public override void OnLeftRoom()
        {
            Debug.Log("<Color=Green>OnLeftRoom(): success</Color>");
            SceneManager.LoadScene(0);
        }

        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            Debug.Log("<Color=Green>OnMasterClientSwitched(): " + newMasterClient.NickName + "</Color>");
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("<Color=Green>OnConnectedToMaster(): success</Color>");
            if (!PhotonNetwork.InLobby) PhotonNetwork.JoinLobby();
        }

        public override void OnJoinedLobby()
        {
            Debug.Log("<Color=Green>OnJoinedLobby(): success</Color>");
        }

        public override void OnCreatedRoom()
        {
            Debug.Log("<Color=Green>OnCreatedRoom(): success</Color>");
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.Log("<Color=Orange>OnCreateRoomFailed(): " + message + " (" + returnCode + ")" + "</Color>");
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("<Color=Green>OnJoinedRoom(): success</Color>");
            this.isConnectingRoom = true;
            SceneManager.LoadScene(1);
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.Log("<Color=Orange>OnJoinRoomFailed(): " + message + " (" + returnCode + ")" + "</Color>");
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("<Color=Orange>OnJoinRandomFailed(): " + message + " (" + returnCode + ")" + "</Color>");
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            this.isConnectingRoom = false;
            Debug.Log("<Color=Orange>OnDisconnected(): " + cause + "</Color>");
        }

        #endregion

        #region Public Methods

        public void ConnectToMaster()
        {
            PhotonNetwork.OfflineMode = false;
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.GameVersion = gameVersion;

            /* PhotonUnityNetworking\Resources\PhotonServerSettings.asset */
            PhotonNetwork.ConnectUsingSettings(); // auto connection based on the config
        }

        #endregion
    }
}