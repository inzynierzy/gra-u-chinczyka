﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PACHISI
{
    public class GameManager : MonoBehaviourPun
    {
        [Header("sprite")]
        public SpriteRenderer spriteRenderer;

        #region MonoBehaviour CallBacks

        void Start()
        {
            if (!PhotonNetwork.IsConnected)
            {
                SceneManager.LoadScene(0);
                return;
            }
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                Debug.Log("PlayerCount: " + PhotonNetwork.CurrentRoom.PlayerCount);
            }

            if (PhotonNetwork.IsMasterClient && Input.GetKeyDown(KeyCode.B))
            {
                string playersNames = "";
                Debug.Log("Master: " + PhotonNetwork.IsMasterClient + " | Players In Room: " + PhotonNetwork.CurrentRoom.PlayerCount + " | RoomName: " + PhotonNetwork.CurrentRoom.Name
                + " Region: " + PhotonNetwork.CloudRegion + " PlayerList: [\n" + playersNames + "]");
                foreach (var playersName in PhotonNetwork.PlayerList)
                {
                    playersNames = playersName + "\n";
                }
            }
        }

        #endregion

        #region Public Methods

        public void StartGameBtn()
        {
            Debug.Log("Start Game Btn");
        }

        public void LeaveRoomBtn()
        {
            PhotonNetwork.LeaveRoom();
        }

        #endregion
    }
}
