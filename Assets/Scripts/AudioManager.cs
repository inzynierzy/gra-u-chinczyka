﻿using UnityEngine.Audio;
using UnityEngine;
using System;

//******USAGE****
// In any script you can add following line of code
//FindObjectOfType<AudioManager>().Play(SoundName);
public class AudioManager : MonoBehaviour

{
    public AudioMixer audioMixer;
    public Sound[] sounds;
    public static AudioManager instance;

    #region Mixer Volume Params
    public static readonly string uiMusicVol = "uiMusicVol";
    public static readonly string uiSfxVol = "uiSfxVol";
    public static readonly string gameMusicVol = "gameMusicVol";
    public static readonly string gameSfxVol = "gameSfxVol";
    #endregion

    public AudioMixer getAudioMixer()
    {
        return this.audioMixer;
    }

    void Awake()
    {
        
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
       foreach( Sound s in sounds)
        {
          s.source =  gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = s.audioMixerGroup;
        }
    }
    private void Start()
    {
        Play("Menu");
    }


    public void Play(string name)
    {
       Sound s =  Array.Find(sounds, sound => sound.name == name);

        if(s != null)
        {
            s.source.Play();
        }
        else
        {
            Debug.LogError("Sound:  " + name + "not found");
        }
        

    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s != null)
        {
            s.source.Stop();
        }
        else
        {
            Debug.LogError("Sound:  " + name + "not found");
        }
    }

    #region Setting Mixer Group Volume
    public void setUiMusicVolume(float volume)
    {
        
        audioMixer.SetFloat(uiMusicVol, volume);
    }

    public void setUiSfxVolume(float volume)
    {
        audioMixer.SetFloat(uiSfxVol, volume);
    }

    public void setGameMusicVolume(float volume)
    {
        audioMixer.SetFloat(gameMusicVol, volume);
    }

    public void setgameSfxVoulme(float volume)
    {
        audioMixer.SetFloat(gameSfxVol, volume);
    }

    #endregion
}
