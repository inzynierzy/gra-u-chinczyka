﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PACHISI
{
    public class ConnectionStatus : MonoBehaviour
    {
        [Header("UI References")]
        public Text connectionStatusText;

        private readonly string connectionStatus = "Status: ";

        #region MonoBehaviour CallBacks

        void Start()
        {

        }

        void Update()
        {
            connectionStatusText.text = connectionStatus + PhotonNetwork.NetworkClientState;
        }

        #endregion
    }
}
