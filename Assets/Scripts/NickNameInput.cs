﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PACHISI
{
    [RequireComponent(typeof(InputField))]
    public class NickNameInput : MonoBehaviour
    {
        public const string nickNamePrefKey = "NickName";

        #region MonoBehaviour CallBacks

        void Start()
        {
            string defaultName = string.Empty;
            InputField _inputField = this.GetComponent<InputField>();

            if (_inputField != null)
            {
                if (PlayerPrefs.HasKey(nickNamePrefKey))
                {
                    defaultName = PlayerPrefs.GetString(nickNamePrefKey);
                    _inputField.text = defaultName;
                }
            }

            PhotonNetwork.NickName = defaultName;
        }

        void Update()
        {

        }

        #endregion

        #region Public Methods

        public void SetNickName(string nickName)
        {
            if (string.IsNullOrEmpty(nickName))
            {
                Debug.Log("<Color=Red>Player Name is null or empty</Color>");
                return;
            }

            PhotonNetwork.NickName = nickName;

            PlayerPrefs.SetString(nickNamePrefKey, nickName);
        }

        #endregion
    }
}
